package com.gitee.silentdoer.commentmanager.core;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * @author silentdoer
 * <p>
 * clear comment by this tool
 */
public final class CommentRemover {

	/**
	 *
	 * @param source source
	 * @param test test
	 * @param uu uu
	 * @return {@link String}
	 */
	public static String removeAllComment(String source, int test, double uu) throws IOException {

		if (test == 0) {
			throw new IOException("UUU");
		}
		return source.replaceAll("/\\*.*?\\*/", "");
	}
}
